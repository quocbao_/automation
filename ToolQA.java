import io.github.bonigarcia.wdm.WebDriverManager;
import net.bytebuddy.asm.Advice;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Actions;


import javax.swing.*;
import java.security.Key;
import java.util.concurrent.TimeUnit;

public class ToolQA {
    WebDriver driver;

//    @BeforeClass
//    public void accessPage() throws InterruptedException {
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\my pc\\WebDriver\\chromedriver.exe");
//        this.driver = new ChromeDriver();
//        this.driver.manage().window().maximize();  // Maximize window
//        this.driver.get("https://demo.guru99.com/test/radio.html"); // Navigate to URL
//        Thread.sleep(3000);
//    }

    @BeforeClass
    public void accessPage() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();  // Maximize window
        String baseUrl = "https://demoqa.com";
        driver.get(baseUrl); // Navigate to URL
        Thread.sleep(3000);
    }

    @Test
    public void textBox() throws InterruptedException {
        Actions action = new Actions(driver);

        driver.findElement(By.id("app"));
        action.click();

        driver.findElement(By.cssSelector("li#item-0")).click();
        driver.findElement(By.id("userName")).sendKeys("Tran Viet Quoc Bao");
        driver.findElement(By.id("userEmail")).sendKeys("quocbao.hat@gmail.com");
        driver.findElement(By.id("currentAddress")).sendKeys("74/25 Vo Van Kiet");
        driver.findElement(By.id("permanentAddress")).sendKeys("Thuy Bieu, Hue");
        Thread.sleep(2000);
        WebElement submitForm = driver.findElement(By.cssSelector("#submit"));
        submitForm.click();
        System.out.println("Submit successfully");
        driver.navigate().refresh();

//        @Test
//        public void checkBox () {
//            driver.findElement(By.id("app")).click();
//            WebElement checkbox = driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[2]"));
//            checkbox.click();
//            WebElement expand = driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/button[1]/*[1]\n"));
//            expand.click();
//            driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[2]/div[2]")).click();
//        }
    }
}
////    @Test
////    public void tryCheckBox() throws InterruptedException {
////        WebElement option1 = driver.findElement(By.id("vfb-6-0"));
////        option1.click();
////        if (option1.isSelected()) {
////            System.out.println("Checkbox is toggled on");
////            Thread.sleep(3000);
////        } else {
////            System.out.println("Checkbox is toggled off");
////            Thread.sleep(3000);
////        }
////        option1.click();
////        if (!option1.isSelected()) {
////            System.out.println("Checkbox is now toggled off");
////        }
////    }
////
////    @Test
////    public void tryRadio() throws InterruptedException {
////        WebElement radio1 = driver.findElement(By.id("vfb-7-1"));
////        WebElement radio2 = driver.findElement(By.id("vfb-7-2"));
////        radio1.click();
////        Thread.sleep(3000);
////        radio2.click();
////        Thread.sleep(3000);
//    }
//}
