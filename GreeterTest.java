import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;

import java.sql.Driver;
import java.util.concurrent.TimeUnit;

// Login to w3.school.com

public class GreeterTest {
    WebDriver driver;

    @BeforeClass
    public void preTest() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();  // Maximize window
        driver.get("https://www.w3schools.com"); // Navigate to URL
        Thread.sleep(2000);
    }

    @Test
    public void testLogin() throws InterruptedException {
        driver.findElement(By.id("w3loginbtn")).click(); // find web element by id
        driver.findElement(By.name("email")).sendKeys("quocbao.hat@gmail.com");
        driver.findElement(By.name("current-password")).sendKeys("Kiwi@231118");

        driver.findElement(By.className("_20LW8")).click(); // find web element by class

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        String profileName = driver.findElement(By.cssSelector("h2#profile-name")).getText();
        System.out.println(profileName);
    }

    @Test
    public void testLogout() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.tagName("button")).click();
    }

}

// Google Search
//    @BeforeClass
//    public void preTest() throws InterruptedException {
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\my pc\\WebDriver\\chromedriver.exe");
//        this.driver = new ChromeDriver();
//        this.driver.manage().window().maximize();  // Maximize window
//        this.driver.get("https://www.google.com"); // Navigate to URL
//        Thread.sleep(2000);
//    }
//    @Test
//    public void search (){
//        driver.findElement(By.name("q")).sendKeys("Automation" + Keys.ENTER); // find web element by className
//    }
//
//    @AfterClass
//    public void postTest(){
//        driver.findElement(By.className("yuRUbf")).click();
//        System.out.println(driver.getTitle());
//        driver.close();
//    }
//}

