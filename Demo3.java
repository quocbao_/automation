import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Demo3 {
    WebDriver driver;

    @BeforeTest
    public void launchBrowser() {
        WebDriverManager.chromedriver().setup();
        // Launch browser
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        // Navigate to page
        String baseUrl = "http://automationpractice.com/index.php?controller=authentication";
        driver.get(baseUrl);
    }

    @Test
    public void signinPage() throws InterruptedException {
        Actions actions = new Actions(driver);

        WebElement signinEmail = driver.findElement(By.id("email"));
        actions.sendKeys(signinEmail,"quocbao.hat@gmail.com").perform();
        WebElement signinPassword = driver.findElement(By.id("passwd"));
        actions.sendKeys(signinPassword,"kiwi231118").perform();

        WebElement signinButton = driver.findElement(By.id("SubmitLogin"));
        actions.click(signinButton).perform();
        Thread.sleep(2000);

        String verifyPageElements = driver.findElement(By.xpath("//div[@class='row addresses-lists']")).getText();
        System.out.println("Elements in My Account section: \n" + verifyPageElements);

        String verifyMainMenu = driver.findElement(By.xpath("//div[@id='block_top_menu']")).getText();
        System.out.println("Main menu include: \n" + verifyMainMenu);

        String verifySubMenu = driver.findElement(By.xpath("//a[@class='sf-with-ul']")).getText();
        System.out.println("Women submenu include: " + verifySubMenu);

        WebElement womenCategory = driver.findElement(By.linkText("Women"));
        actions.click(womenCategory).perform();
        Thread.sleep(2000);

        WebElement tops = driver.findElement(By.linkText("Tops"));
        actions.click(tops).perform();
        Thread.sleep(2000);

        WebElement product = driver.findElement(By.partialLinkText("Faded"));
        actions.click(product).perform();
        Thread.sleep(2000);

        WebElement addToCart = driver.findElement(By.name("Submit"));
        actions.click(addToCart).perform();
        Thread.sleep(2000);

        WebElement proceedToCheckout = driver.findElement(By.partialLinkText("Proceed"));
        actions.moveToElement(proceedToCheckout);
        actions.click(proceedToCheckout).perform();
        Thread.sleep(2000);

        WebElement signOut = driver.findElement(By.linkText("Sign out"));
        actions.click(signOut).perform();
        Thread.sleep(2000);

        WebElement alertMessage = driver.findElement(By.xpath("//p[contains(text(),'Your shopping cart is empty.')]"));
        String actualMessage = alertMessage.getText();
        Assert.assertEquals(actualMessage,"Your shopping cart is empty.");
    }
}