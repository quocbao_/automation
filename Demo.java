import io.github.bonigarcia.wdm.WebDriverManager;
import org.bouncycastle.asn1.dvcs.DVCSObjectIdentifiers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;


import javax.management.BadStringOperationException;
import java.util.Arrays;
import java.util.List;

public class Demo {
    WebDriver driver;

    @BeforeTest
    public void navigatePage() throws InterruptedException {
        //Installation driver
        WebDriverManager.chromedriver().setup();
        // Launch browser
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        // Navigate to page
        final String baseUrl = "https://admin-demo.nopcommerce.com/login";
        driver.get(baseUrl);
        Thread.sleep(2000);
    }

    @BeforeMethod
    public void login() throws InterruptedException {
        Actions actions = new Actions(driver);
        // Find Email text box and clear filled information
        WebElement emailAddress = driver.findElement(By.cssSelector("#Email"));
        emailAddress.clear();
        emailAddress.sendKeys("admin@yourstore.com");

        WebElement passwordEle = driver.findElement(By.cssSelector("#Password"));
        passwordEle.clear();
        passwordEle.sendKeys("admin");

        WebElement loginElem = driver.findElement(By.cssSelector("[type='submit']"));
        loginElem.click();
    }
    // Test case 1: Create new category

    //    @Test
//    public void createNewCategory() throws InterruptedException {
//        Actions actions = new Actions(driver);
//        // Verify left menu
//        Boolean actualMenu = driver.findElement(By.className("os-padding")).isDisplayed();
//        System.out.println("Verify left menu is displayed: " + actualMenu);
//        Thread.sleep(2000);
//        // Mouse hover and click on Catalog in left menu
//        WebElement catalog = driver.findElement(By.xpath("//body[1]/div[3]/aside[1]/div[1]/div[4]/div[1]/div[1]/nav[1]/ul[1]/li[2]/a[1]/p[1]"));
//        actions.moveToElement(catalog);
//        actions.click(catalog).build().perform();
//        Thread.sleep(1000);
//        // Mouse hover and click on Categories
//        WebElement categories = driver.findElement(By.xpath("//p[contains(text(),'Categories')]"));
//        actions.moveToElement(categories);
//        actions.click(categories).build().perform();
//        Thread.sleep(1000);
//        //Verify buttons
//        Boolean buttonsDisplayed = driver.findElement(By.xpath("//body/div[3]/div[1]/div[1]/div[1]")).isDisplayed();
//        System.out.println("All buttons is displayed: " + buttonsDisplayed);
//        Boolean buttonsEnabled = driver.findElement(By.xpath("//body/div[3]/div[1]/div[1]/div[1]")).isEnabled();
//        System.out.println("All button is enabled: " + buttonsEnabled);
//        String buttonsName = driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[1]/div[1]")).getText();
//        System.out.println("Buttons: " + buttonsName);
//        // Verify "Category name" text box
//
//        //Verify "Published" dropdown list
//        Select s = new Select(driver.findElement(By.xpath("//select[@id='SearchPublishedId']")));
//        List<WebElement> options = s.getOptions();
//        int size = options.size();
//        for (int i = 0; i < size; i++) {
//            String dropdown = options.get(i).getText();
//            System.out.println(dropdown);
//        }
//        // Click on "Add new" button
//        WebElement addNewButton = driver.findElement(By.xpath("//body/div[3]/div[1]/div[1]/div[1]/a[1]/i[1]"));
//        actions.moveToElement(addNewButton);
//        actions.click().build().perform();
//        Thread.sleep(2000);
//        // Input value into "Name" text box
//        WebElement productName = driver.findElement(By.xpath("//input[@id='Name']"));
//        actions.sendKeys(productName, "Phone").build().perform();
//        Thread.sleep(2000);
//        // Click "Save" button
//        WebElement saveButton = driver.findElement(By.xpath("//body/div[3]/div[1]/form[1]/div[1]/div[1]/button[1]"));
//        saveButton.submit();
//        Thread.sleep(2000);
//        // Verify message
//        String actualMessage = driver.findElement(By.xpath("//body/div[3]/div[1]/div[1]")).getText();
//        String expectedMessage = "×\nThe new product has been added successfully.";
//        if (expectedMessage.equals(actualMessage)) {
//            System.out.println("Verify add new product message: Correct message");
//        } else {
//            System.out.println("Verify add new product message: Correct message");
//        }
//    }
//
    @AfterMethod
    public void logOut() throws InterruptedException {
        Actions actions = new Actions(driver);
        WebElement logout = driver.findElement(By.xpath("//a[contains(text(),'Logout')]"));
        actions.moveToElement(logout);
        actions.click().build().perform();
        Thread.sleep(2000);
    }

    // Test case 2: Create new customer
    @Test
    public void createNewCustomers() throws InterruptedException {
        Actions actions = new Actions(driver);
        // Verify left menu
        Boolean actualMenu = driver.findElement(By.className("os-padding")).isDisplayed();
        System.out.println("Verify left menu is displayed: " + actualMenu);
        Thread.sleep(2000);

        // Mouse hover and click on Catalog in left menu
        WebElement catalog = driver.findElement(By.xpath("//body[1]/div[3]/aside[1]/div[1]/div[4]/div[1]/div[1]/nav[1]/ul[1]/li[2]/a[1]/p[1]"));
        actions.moveToElement(catalog);
        actions.click(catalog).build().perform();
        Thread.sleep(1000);

        // Mouse hover and click on Products
        WebElement products = driver.findElement(By.xpath("//body[1]/div[3]/aside[1]/div[1]/div[4]/div[1]/div[1]/nav[1]/ul[1]/li[2]/ul[1]/li[1]/a[1]/p[1]"));
        actions.moveToElement(products);
        actions.click(products).build().perform();
        Thread.sleep(1000);

        //Verify buttons
        Boolean buttonsDisplayed = driver.findElement(By.xpath("//body/div[3]/div[1]/form[1]/div[1]/div[1]")).isDisplayed();
        System.out.println("All buttons is displayed: " + buttonsDisplayed);
        Boolean buttonsEnabled = driver.findElement(By.xpath("//body/div[3]/div[1]/form[1]/div[1]/div[1]")).isEnabled();
        System.out.println("All buttons is enabled: " + buttonsEnabled);

        String buttonsName = driver.findElement(By.xpath("//body/div[3]/div[1]/form[1]/div[1]/div[1]")).getText();
        System.out.println("Buttons: \n" + buttonsName);

        // Verify all fields
        String fields = driver.findElement(By.xpath("//body/div[3]/div[1]/form[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]")).getText();
        System.out.println("All fields: \n" + fields);

        // Click on "Add new" button
        WebElement addNewButton = driver.findElement(By.xpath("//body/div[3]/div[1]/form[1]/div[1]/div[1]/a[1]"));
        actions.moveToElement(addNewButton);
        actions.click().build().perform();
        Thread.sleep(2000);

        // Put value into "Product name"
        WebElement productName = driver.findElement(By.xpath("//input[@id='Name']"));
        actions.sendKeys(productName, "Samsung A52").build().perform();
        Thread.sleep(2000);

        // Put value into "Short description"
        WebElement shortDescription = driver.findElement(By.xpath("//textarea[@id='ShortDescription']"));
        actions.sendKeys(shortDescription, "128GB Memory, RAM 8GB").build().perform();
        Thread.sleep(2000);

        // Put value into "SKU"
        WebElement sku = driver.findElement(By.xpath("//input[@id='Sku']"));
        actions.sendKeys(sku, "SS001").build().perform();
        Thread.sleep(2000);

//        // Put value into "Categories"
//        WebElement categories = driver.findElement(By.id("SelectedCategoryIds"));
//        Select multiplechoicelist = new Select(categories);
//        multiplechoicelist.selectByValue("1");
//        multiplechoicelist.selectByIndex(0);
//        Thread.sleep(2000);


        // Put value into "Price"
        WebElement price = driver.findElement(By.xpath("//input[@id='Price']//preceding-sibling::input"));
        actions.sendKeys(price, "300").build().perform();
        Thread.sleep(2000);

        // Put value into "Old price"
        WebElement oldPrice = driver.findElement(By.xpath("//input[@id='OldPrice']//preceding-sibling::input"));
        actions.sendKeys(oldPrice, "400").build().perform();
        Thread.sleep(2000);

        // Click "Save" button
        WebElement saveData = driver.findElement(By.name("save"));
        saveData.submit();
        Thread.sleep(2000);

//      Verify message
        String actualMessage = driver.findElement(By.xpath("//body/div[3]/div[1]/div[1]")).getText();
        String expectedMessage = "×\nThe new product has been added successfully.";
        if (expectedMessage.equals(actualMessage)) {
            System.out.println("Verify add new product message: Correct message");
        } else {
            System.out.println("Verify add new product message: Correct message");
            System.out.println("ABC");
            System.out.println("Tui vừa sửa code nè");

        }
    }
}




