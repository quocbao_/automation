import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.annotation.concurrent.ThreadSafe;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Demo2 {
    WebDriver driver;
@BeforeTest
public void launchBrowser() {
    //Installation driver
    WebDriverManager.chromedriver().setup();
    // Launch browser
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    // Navigate to page
    String baseUrl = "http://automationpractice.com/index.php";
    driver.get(baseUrl);
}
    @Test
        public void signinPage() throws InterruptedException {
        Actions actions = new Actions(driver);

        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        WebElement signIn = driver.findElement(By.linkText("Sign in"));
        actions.moveToElement(signIn).click().build().perform();
        Thread.sleep(2000);

        WebElement emailAddress = driver.findElement(By.xpath("//input[@id='email_create']"));
        actions.sendKeys(emailAddress,"kpmzvuigaujgiswluw@kvhrr.com").perform();
        Thread.sleep(1000);

        WebElement createButton = driver.findElement(By.xpath("//button[@id='SubmitCreate']"));
        createButton.submit();
        Thread.sleep(1000);

        WebElement genderMale = driver.findElement(By.xpath("//input[@id='id_gender1']"));
        actions.click(genderMale).perform();
        Thread.sleep(1000);

        // Input first name
        WebElement firstName = driver.findElement(By.xpath("//input[@id='customer_firstname']"));
        actions.sendKeys(firstName,"Quoc Bao").perform();
        Thread.sleep(2000);

        WebElement lastName = driver.findElement(By.xpath("//input[@id='customer_lastname']"));
        actions.sendKeys(lastName,"Tran Viet").perform();
        Thread.sleep(2000);

        WebElement passWord = driver.findElement(By.xpath("//input[@id='passwd']"));
        actions.sendKeys(passWord,"kiwi231118").perform();
        Thread.sleep(2000);

        WebElement days = driver.findElement(By.xpath("//select[@id='days']"));
        Select selectDays = new Select(days);
        selectDays.selectByValue("3");
        Thread.sleep(1000);

        WebElement months = driver.findElement(By.xpath("//select[@id='months']"));
        Select selectMonths = new Select(months);
        selectMonths.selectByValue("10");
        Thread.sleep(1000);

        WebElement years = driver.findElement(By.xpath("//select[@id='years']"));
        Select selectYears= new Select(years);
        selectYears.selectByValue("1991");
        Thread.sleep(1000);

        WebElement address = driver.findElement(By.id("address1"));
        actions.sendKeys(address,"Vo Van Kiet").perform();
        Thread.sleep(1000);

        WebElement city = driver.findElement(By.id("city"));
        actions.sendKeys(city,"Da Nang").perform();
        Thread.sleep(1000);


        WebElement state = driver.findElement(By.id("id_state"));
        Select selectState= new Select(state);
        selectState.selectByValue("1");
        Thread.sleep(1000);

        // List all value in State dropdown list
        List<WebElement> stateOptions = selectState.getOptions();
        System.out.println(stateOptions.size());

        for(WebElement e:stateOptions)
        {
            System.out.println("The value of state are: " +e.getText());
        }

        WebElement zipCode = driver.findElement(By.id("postcode"));
        actions.sendKeys(zipCode,"50000").perform();
        Thread.sleep(1000);

        WebElement country = driver.findElement(By.id("id_country"));
        Select selectCountry= new Select(country);
        selectCountry.selectByValue("21");
        Thread.sleep(1000);

        WebElement phone = driver.findElement(By.id("phone_mobile"));
        actions.sendKeys(phone,"0901901778").perform();
        Thread.sleep(1000);

        WebElement addressAlias = driver.findElement(By.id("alias"));
        actions.sendKeys(addressAlias,"74/25 Vo Van Kiet").perform();
        Thread.sleep(1000);

        WebElement register = driver.findElement(By.id("submitAccount"));
        register.submit();
        Thread.sleep(4000);
        //
    }
}


